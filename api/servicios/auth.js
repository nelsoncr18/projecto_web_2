const Users = require('../modelos').User;
const config = require('../config');
const CustomError = require('../CustomError');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

const authenticate = params => {
	return Users.findOne({
		where: {
			login: params.login
		},
		raw: true
	}).then(user => {
		if (!user)
			throw new CustomError('Authenticacion fallida, usuario no registrado.');

		if (!bcrypt.compareSync(params.password || '', user.password))
			throw new CustomError('Authenticacion fallida, contraseña incorrecta.');

		const payload = {
			login: user.login,
			id: user.id,
			time: new Date()
		};

		var token = jwt.sign(payload, config.jwtSecret, {
			expiresIn: config.tokenExpireTime
		});

		return token;
	});
}

module.exports = {
	authenticate
}